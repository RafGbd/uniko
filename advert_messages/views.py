from django.shortcuts import get_object_or_404
from .serializers import MessageSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from .models import Message
from users.models import User
from rest_framework.decorators import detail_route
from django.http import JsonResponse
from django.views.generic.list import ListView
from django.views.generic import TemplateView
from django.views.generic import View

from django.db.models import Q
from django.db.models import Max

class MessageViewSet(viewsets.ModelViewSet):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
    # permission_classes = (IsAuthenticated,)

    def list(self, request):
        queryset = Message.objects.all()
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Message.objects.all()
        adv = get_object_or_404(queryset, pk=pk)
        serializer = MessageSerializer(adv)
        return Response(serializer.data)

    @detail_route(methods=("get",))
    def messages_by_sender(self, request, pk):
        queryset = Message.objects.filter(author__id=pk)
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)

    @detail_route(methods=("get",))
    def messages_by_advert(self, request, pk):
        queryset = Message.objects.filter(advert__id=pk)
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)

    @detail_route(methods=("get",))
    def messages_to_user(self, request, pk):
        queryset = Message.objects.filter(user__id=pk)
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)


class MessageInbox(ListView):
    model = Message
    allow_empty = True
    template_name = 'advert_messages/message_list.html'

    def get_queryset(self, **kwargs):
        user = self.request.user.profile
        messages = []
        recent_conversations = Message.objects.filter(
            Q(author_id=user.id) | Q(user_id=user.id)
        ).values('conversation_id').annotate(
            last_created=Max('created_at')
        ).order_by('-last_created')

        for conversation in recent_conversations:
            message = Message.objects.filter(
                conversation_id=conversation['conversation_id']
            ).order_by('-created_at').first()
            
            if message.author_id == user.id:
                message.chat_id = message.user_id
            else:
                message.chat_id = message.author_id
            messages.append(message)

        return messages


class ChatView(TemplateView):
    model = Message
    allow_empty = True
    template_name = 'advert_messages/message.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(ChatView, self).get_context_data(*args, **kwargs)
        ctx['other_guy_pk'] = self.kwargs['other_guy_pk']
        return ctx


class ChatDataView(View):
    def get(self, request, other_guy_pk):
        user = request.user.profile
        last_id = request.GET.get('last_id')

        messages = []
        messages_qs = Message.objects.filter(
            Q(author_id=user.id) | Q(user_id=user.id)
        ).filter(
            Q(author_id=other_guy_pk) | Q(user_id=other_guy_pk)
        ).order_by('created_at')

        if last_id:
            messages_qs = messages_qs.filter(id__gt=last_id)

        messages_qs.update(was_read=True)
        for message in messages_qs:
            messages.append({
                'id': message.id,
                'author_id': message.author_id,
                'message_text': message.message_text,
                'avatar': message.author.avatar_url
            })
        return JsonResponse({'messages': messages})


class MessageCreate(View):
    def post(self, request):
        user = request.user.profile
        message_text = request.POST.get('message_text')
        user_id = request.POST.get('user_id')
        if not all([user_id, message_text]):
            return JsonResponse({'error': 1})

        message = Message(
            message_text=message_text,
            user_id=user_id,
            author=user
        )
        message.save()
        return JsonResponse({'error': 0})
