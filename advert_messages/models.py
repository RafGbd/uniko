from django.db import models
from users.models import UserProfile
from board.models import Advert
from django.utils.translation import ugettext_lazy as _


class Message(models.Model):
    author = models.ForeignKey(UserProfile, related_name="my_messages")
    advert = models.ForeignKey(Advert, null=True)
    user = models.ForeignKey(UserProfile, related_name="messages_to_me")
    message_text = models.TextField(_("Text"))
    created_at = models.DateTimeField(auto_now_add=True)
    was_read = models.BooleanField(default=False)
    conversation_id = models.CharField(max_length=90, null=True)

    class Meta:
        verbose_name = _("Message")
        verbose_name_plural = _("Messages")
        ordering = ["created_at"]

    def __str__(self):
        return "{}:{}".format(self.author, self.message_text)

    def save(self, *args, **kwargs):
        if self.advert is not None:
            self.user = self.advert.owner
        if not self.conversation_id:
            conv_id = "_".join(map(str, sorted([int(self.author_id), int(self.user_id)])))
            self.conversation_id = conv_id
        super(Message, self).save(*args, **kwargs)
