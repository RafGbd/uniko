from .views import MessageViewSet
from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.MessageInbox.as_view(), name="messages"),
    url(r'^create/$', views.MessageCreate.as_view(), name="message_create"),
    url(r'^(?P<other_guy_pk>[0-9]+)/$', views.ChatView.as_view(), name="chat"),
    url(r'^(?P<other_guy_pk>[0-9]+)/data/$', views.ChatDataView.as_view(), name="chat_data")
]

# router = DefaultRouter()
# router.register(r'api', MessageViewSet)
# urlpatterns += router.urls
