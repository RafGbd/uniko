from django.views.generic.base import TemplateView
from django.shortcuts import redirect, render


class Homepage(TemplateView):
    template_name = "landing.html"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect("advert-list")
        else:
            return render(request, self.template_name)

    
