
from django.forms import ModelForm, inlineformset_factory
from .models import Advert, AdvertImages


class AdvertForm(ModelForm):
    class Meta:
        model = Advert
        fields = ("title", "description", "price", "item_condition", "hidden", "category")


class AdvertImagesForm(ModelForm):
    class Meta:
        model = AdvertImages
        fields = "__all__"

ImageFormset = inlineformset_factory(
    Advert, AdvertImages, fields=("image",), extra=1)
