from django.contrib import admin
from .models import Advert, AdvertImages, Category

# admin.site.register(Stakes)
admin.site.register(Advert)
admin.site.register(AdvertImages)
admin.site.register(Category)
