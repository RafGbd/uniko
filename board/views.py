from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from .models import Advert, AdvertImages, Category
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .forms import AdvertForm, ImageFormset
from django.contrib.auth.mixins import LoginRequiredMixin
from .serializers import AdvertSerializer, FullAdvertSerial
from rest_framework import generics, status
from django.views.generic.detail import DetailView
from rest_framework.response import Response
from advert_messages.models import Message
from django.urls import reverse
from django.core.exceptions import PermissionDenied
from django.db.models import Q


class AdvertListView(LoginRequiredMixin, ListView):
    model = Advert
    template_name = "board/index.html"

    def get_queryset(self, **kwargs):
        get_query = self.request.GET.get("search_query")

        try:
            category_id = self.args[0]
        except Exception as e:
            category_id = None
        if category_id:
            queryset = Advert.objects.on_sale().filter(
                owner__campus=self.request.user.profile.campus, category__id=category_id)
        else:
            queryset = Advert.objects.on_sale().filter(
                owner__campus=self.request.user.profile.campus)

        if get_query:
            queryset = queryset.filter(Q(title__icontains=get_query) | Q(
                description__icontains=get_query))

        return queryset

    def get_context_data(self):
        context = super(AdvertListView, self).get_context_data()
        context["categories"] = Category.objects.all()
        return context


class AdvertCreate(CreateView):
    model = Advert
    form_class = AdvertForm
    success_url = reverse_lazy('advert-list')

    def form_valid(self, form):
        form.instance.owner = self.request.user.profile
        self.object = form.save()
        files = self.request.FILES.getlist('files[]')
        for file in files:
            image_obj = AdvertImages(advert=self.object)
            image_obj.image.save(file.name, file)
            image_obj.save()
        return redirect('advert-detail', self.object.pk)


class AdvertUpdate(UpdateView):
    model = Advert
    form_class = AdvertForm
    template_name = "board/advert_update.html"

    def get_success_url(self, **kwargs):
        view_name = 'advert-detail'
        return reverse(view_name, kwargs={'pk': self.object.id})

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.owner = self.request.user.profile

        instance.save()
        form.save_m2m()

        files = self.request.FILES.getlist('files[]')
        for file in files:
            image_obj = AdvertImages(advert=self.object)
            image_obj.image.save(file.name, file)
            image_obj.save()

        return redirect('advert-detail', self.object.pk)


class AdvertDelete(DeleteView):
    model = Advert
    success_url = reverse_lazy('/')


class AdvertDetail(DetailView):
    model = Advert
    fields = "__all__"

    def get_context_data(self, **kwargs):
        obj = kwargs.get("object")
        context = super(AdvertDetail, self).get_context_data(**kwargs)
        context['images'] = AdvertImages.objects.filter(advert=obj)
        return context


class AdvertSoldView(generics.RetrieveAPIView):
    model = Advert
    serializer_class = AdvertSerializer
    queryset = Advert.objects.all()


class AdvertSoldUpdate(generics.UpdateAPIView):
    model = Advert
    serializer_class = FullAdvertSerial
    queryset = Advert.objects.all()

    def update(self, request, *args, **kwargs):
        obj = get_object_or_404(Advert, pk=kwargs["pk"])
        if obj is None:
            return Response(status=status.HTTP_404_NOT_FOUND)
        # obj.sold = True
        # obj.sold_to = UserProfile.objects.all().first()
        obj.save()
        Message.objects.create(
            user=self.owner, author=self.sold_to, message_text="Sold", advert=self.id)
        return Response("201")
