# from .views import AdvertViewSet, StakesViewSet
# from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.AdvertListView.as_view(), name="advert-list"),
    url(r'^([0-9]+)/$', views.AdvertListView.as_view(), name="advert-list-cats"),
    url(r'^add_advert/$', views.AdvertCreate.as_view(), name="add-advert"),
    url(r'^edit_advert/(?P<pk>[0-9]+)/$',
        views.AdvertUpdate.as_view(), name="edit-advert"),
    url(r'^del_advert/(?P<pk>[0-9]+)/$',
        views.AdvertDelete.as_view(), name="delete-advert"),
    url(r'^advert/(?P<pk>[0-9]+)/$',
        views.AdvertDetail.as_view(), name="advert-detail"),
    url(r"^advert/(?P<pk>[0-9]+)/check_sold/$",
        views.AdvertSoldView.as_view(), name="advert_check_sold"),
    url(r"^advert/(?P<pk>[0-9]+)/sold", views.AdvertSoldUpdate.as_view(), name="advert_sold"),
]


# router = DefaultRouter()
# router.register(r'advert', AdvertViewSet)
# router.register(r'stakes', StakesViewSet)

# urlpatterns = router.urls
