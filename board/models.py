from django.db import models
from users.models import UserProfile
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

class Category(models.Model):
    name = models.CharField(_("Category"), max_length=100)
    pictogram = models.CharField(_("Pict"), max_length=50, blank=True)

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.name


class AdvertManager(models.Manager):
    def get_queryset(self):
        return super(AdvertManager, self).get_queryset()

    def on_sale(self):
        return super(AdvertManager, self).get_queryset().filter(sold=False, hidden=False)


class Advert(models.Model):

    CONDITIONS = (("used", "Used"), ("like_new", "Like new"), ("new", "New"))

    owner = models.ForeignKey(UserProfile, verbose_name=_("Seller"))
    title = models.CharField(_("Title"), max_length=100)
    description = models.TextField(_("Description"))
    created_at = models.DateTimeField(auto_now_add=True)
    price = models.DecimalField(
        _("Price"), max_digits=10, decimal_places=2, default=0)
    item_condition = models.CharField(
        _("Condition"), max_length=8, choices=CONDITIONS, blank=True, null=True)
    sold = models.BooleanField(_("Sold"), default=False)
    sold_to = models.ForeignKey(UserProfile, verbose_name=_(
        "Buyer"), related_name=_("Buyer"), null=True, blank=True)
    hidden = models.BooleanField(_("Hide"), default=False)
    category = models.ForeignKey(Category)

    objects = AdvertManager()

    class Meta:
        verbose_name = _("Advert")
        verbose_name_plural = _("Adverts")
        ordering = ["created_at"]

    def get_absolute_url(self):
        return reverse('advert-detail', kwargs={'pk': self.id})

    @property
    def main_image(self):
        img = AdvertImages.objects.filter(advert=self).first()
        if img:
            return img.image.url
        else:
            return ""  # static No-image-placeholder

    def __str__(self):
        return "{}".format(self.title)


class AdvertImages(models.Model):
    advert = models.ForeignKey(Advert)
    image = models.ImageField(upload_to='adverts')
