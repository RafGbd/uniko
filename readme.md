# README #

### How do I get set up? ###

1. Установить проект и все его зависимости в виртуальное окружение, убедиться что ./manage.py runserver работает
2. Создать суперюзера по своему вкусу.
3. Зайти в админку localhost:8000/admin и выполнить следующее:
	3.1. Создать кампус. Данные на усмотрение.
	3.2. Создать один или несколько доменов, ассоциированных с кампусом
	3.3. Создать профиль юзера (User Profiles), указать там кампус, системного юзера и email одного из кампусовых доменов.
4. Выйти из админки и попытаться залогиниться под админом в обычный интерфейс.
5. Для размещения товаров нужно в админке вначале задать категории. Это сделано для того, чтобы админ мог расширять список категорий без услуг программиста.
6. После категорий можно создавать объявления о продаже через обычный интерфейс