"""adv_board URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from landing import views
from registration.backends.hmac.views import RegistrationView
from users.forms import UnikoRegistrationForm
from users.views import UnikoRegistrationView
from django.views.generic.base import TemplateView


urlpatterns = [
    url(r"^$", views.Homepage.as_view(), name="landing"),
    url(r'^admin/', admin.site.urls),
    url(r'^messages/', include("advert_messages.urls")),
    url(r'^users/', include("users.urls")),
    url(r'^board/', include("board.urls")),
    url(r'^accounts/register/$', UnikoRegistrationView.as_view(form_class=UnikoRegistrationForm),
        name='registration:registration_register'),
    url(r'^accounts/', include('registration.backends.hmac.urls', namespace="registration")),
    url(r'^users/profiles', views.Homepage.as_view(), name="registration_complete"),
    url(r'^users/profiles', views.Homepage.as_view(), name="registration_activation_complete"),
    url(r'^privacy_policy$', TemplateView.as_view(template_name='privacy_policy.html'), name='privacy_policy'),
    url(r'^terms_of_service$', TemplateView.as_view(template_name='terms_of_service.html'), name='terms'),
    url(r'^about$', TemplateView.as_view(template_name='about.html'), name='about'),
    url(r'^faq$', TemplateView.as_view(template_name='faq.html'), name='faq'),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
