from django.views.generic.detail import DetailView
from .models import Review, UserProfile
from django.views.generic.edit import UpdateView
from django.shortcuts import reverse, redirect
from .serializers import ReviewSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from django.core.exceptions import PermissionDenied
from registration.backends.hmac.views import RegistrationView
from django.contrib.auth.models import User
from users.forms import UnikoRegistrationForm, UserProfileForm


class ReviewViewSet(viewsets.ModelViewSet):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()

    # def list(self, request):
    #     queryset = Review.objects.all()
    #     serializer = ReviewSerializer(queryset, many=True)
    #     return Response(serializer.data)

    def retrieve(self, request, pk):
        queryset = Review.objects.filter(user__id=pk)
        serializer = ReviewSerializer(queryset, many=True)
        return Response(serializer.data)


class UserDetail(DetailView):
    model = UserProfile
    template_name = "users/profile.html"
    fields = "__all__"


class UserUpdate(UpdateView):
    model = UserProfile
    form_class = UserProfileForm
    template_name = "users/update.html"

    def form_valid(self, form):
        if 'password' in form.cleaned_data and form.cleaned_data['password']:
            self.request.user.set_password(form.cleaned_data['password'])
            self.request.user.save()
        return super(UserUpdate, self).form_valid(form)

    def get_success_url(self):
        return reverse('user_profile', args=[self.object.id])

    def get_object(self, *args, **kwargs):
        if int(self.kwargs.get("pk", 0)) != self.request.user.profile.id:
            raise PermissionDenied("try to change other's people details, bad boy?")
        else:
            return super(UserUpdate, self).get_object()


class UnikoRegistrationView(RegistrationView):
    form_class = UnikoRegistrationForm

    def register(self, form):
        user = User(username=form.data["username"],
            email=form.data["email"])
        user.set_password(form.data["password1"])
        user.active = False
        user.save()
        # send_mail with rendered template
        return user


