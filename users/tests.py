from django.test import TestCase
from .models import Campus, CampusDomain, UserProfile, Review
from django.contrib.auth.models import User


class CampusTest(TestCase):

    def setUp(self):
        self.htul = Campus.objects.create(name="Cthulhu college")

    def test_domain_edu(self):
        d1 = CampusDomain.objects.create(
            campus=self.htul, domain="cthulhu.edu")
        self.assertTrue(CampusDomain.objects.filter(
            domain="cthulhu.edu").first())
        d2 = CampusDomain.objects.create(
            campus=self.htul, domain="cthulhu.non")
        self.assertTrue(CampusDomain.objects.filter(
            domain="cthulhu.non").first())

    def test_user_get_campus(self):
        CampusDomain.objects.create(campus=self.htul, domain="cthulhu.edu")
        u1 = User.objects.create(username="maclou", email="doctor@cthulhu.edu")
        self.assertTrue(UserProfile.objects.filter(user=u1).first())


class ReviewTest(TestCase):

    def setUp(self):
        self.htul = Campus.objects.create(name="Cthulhu college")
        CampusDomain.objects.create(campus=self.htul, domain="cthulhu.edu")
        self.user_one = User.objects.create(
            username="maclou", email="doctor@cthulhu.edu")
        self.user_two = User.objects.create(
            username="lennon", email="ftagn@cthulhu.edu")

    def test_selfreview(self):
        rev = Review.objects.create(
            user=self.user_one.profile, author=self.user_one.profile, text="Great guy!", rating=5)
        self.assertIsNone(rev)
