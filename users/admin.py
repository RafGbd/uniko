from django.contrib import admin
from .models import UserProfile, Review, Campus, CampusDomain

admin.site.register(UserProfile)
admin.site.register(Review)
admin.site.register(Campus)
admin.site.register(CampusDomain)