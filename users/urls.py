from .views import ReviewViewSet
from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<pk>[0-9]+)/update$',
        views.UserUpdate.as_view(), name="update_profile"),
    url(r'^(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name="user_profile"),

]

router = DefaultRouter()
router.register(r'reviews', ReviewViewSet)
urlpatterns += router.urls
