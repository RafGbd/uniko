from registration.forms import RegistrationFormTermsOfService, RegistrationFormUniqueEmail
from django import forms
from .models import CampusDomain, Campus, UserProfile
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class UnikoRegistrationForm(RegistrationFormUniqueEmail, RegistrationFormTermsOfService):
    colledge = forms.ModelChoiceField(queryset=Campus.objects.all())

    def is_valid(self):
        mail = self.data["email"]
        if mail:
            domain = mail.split("@")[1]
            searched_domain = CampusDomain.objects.filter(
                domain=domain).first()
            if not searched_domain:
                self.add_error("email", "Your email not allowed")
                return False
            return True

        return super(UnikoRegistrationForm, self).is_valid()

    class Meta:
        model = User
        fields = ["username", "email", "password1", "tos", "colledge"]


class UserProfileForm(forms.ModelForm):
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
        required=False
    )

    class Meta:
        model = UserProfile
        fields = ("name", "surname", "description", "avatar")
