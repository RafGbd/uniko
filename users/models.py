from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.db.models import Avg
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings



class Campus(models.Model):
    name = models.CharField(_("Campus name"), max_length=100)

    class Meta:
        verbose_name = _("Campus")
        verbose_name_plural = _("Campuses")

    def __str__(self):
        return self.name


class CampusDomain(models.Model):
    campus = models.ForeignKey(Campus, related_name="emails")
    domain = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        if not self.domain.endswith("edu") and not settings.DEBUG:
            return
        super(CampusDomain, self).save(*args, **kwargs)

    def __str__(self):
        return "{}:{}".format(self.campus, self.domain)


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name="profile")
    name = models.CharField(_("First name"), max_length=100, blank=True)
    surname = models.CharField(_("Surname"), max_length=100, blank=True)
    avatar = models.ImageField(null=True, blank=True)
    campus = models.ForeignKey(Campus)
    email = models.EmailField()
    description = models.TextField(_("Description"), blank=True)

    def __str__(self):
        return self.user.username

    def new_messages_count(self):
        return self.messages_to_me.filter(was_read=False).count()

    @property
    def average_rating(self):
        rate = Review.objects.filter(user=self).aggregate(Avg('rating')).get("rating_avg", 0.00)
        return rate

    @property
    def media_directory_path(self):
        return '/users/{}'.format(self.user.username)

    @property
    def avatar_url(self):
        if self.avatar:
            return self.avatar.url
        else:
            return static('img/user')


class Review(models.Model):
    user = models.ForeignKey(UserProfile)
    author = models.ForeignKey(UserProfile, related_name="Author")
    rating = models.PositiveSmallIntegerField(_("Rating"))
    text = models.TextField(_("Review"))
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _("Review")
        verbose_name_plural = _("Reviews")
        ordering = ["created_at"]

    def save(self, *args, **kwargs):
        if not settings.DEBUG:
            if self.user == self.author:
                raise Exception("You can't review yourself")
            if Review.objects.filter(user=self.user, author=self.author).first():
                raise Exception("You already review this user")
        super(Review, self).save(*args, **kwargs)

    def __str__(self):
        return "{} to {}: {}".format(self.author, self.user, self.rating)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created and not instance.is_staff:
        domain = instance.email.split("@")[1]
        searched_domain = CampusDomain.objects.filter(domain=domain).first()
        if searched_domain:
            UserProfile.objects.create(
                user=instance, campus=searched_domain.campus, email=instance.email)
